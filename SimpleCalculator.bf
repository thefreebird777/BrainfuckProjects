ENTER EQUATION IN REVERSE POLISH NOTATION
INPUT TWO POSITIVE SINGLE DIGIT NUMBERS THAT RESULT IN A POSITIVE SINGLE DIGIT RESULT
FOLLOWED BY A PLUS OR MINUS SIGN (First number must be larger when subtracting.)

>,>, two number inputs
<<   Reset pointer

subtracts 48 from both inputs
+++ +++                        loops 6 times
[
  >---- ----
  >---- ----
  <<-
]

>>>>,   sign input (plus ascii value is 43 and minus ascii value is 45)

subtracts 43 from the last input
<<<< +++ +++                    loops 6 times
[
 >>>> ---- ---
 <<<<-
]
>>>> -

========== Ben_Arba IF STATEMENT ==========
This checks if the input minus 43 is zero

>temp0[-]+         val is 1
>temp1[-]          val is 0
x<<[
 temp0>-]
>         temp0 or temp1 depending if it enters previous loop
[
 <<<<
 [      loops for first input
   >+     adds 1 to second input
   <-
 ]
 adds 48 to result
 +++ +++                        loops 6 times
 [
  >++++ ++++
  <-
 ]
 >. Prints result
 temp0>->
]<<

adds 43 to the last input (so that the input is the original value)
<<<< +++ +++                   loops 6 times
[
 >>>> ++++ +++
 <<<<-
]
>>>> +

subtracts 45 from the last input
<<<< +++ ++                     loops 5 times
[
>>>> ---- ---- -
<<<<-
]
>>>>
========== Ben_Arba IF STATEMENT ==========
This checks if the input minus 45 is zero

>temp0[-]+         val is 1
>temp1[-]          val is 0
x<<[
 temp0>-]
>         temp0 or temp1 depending if it enters previous loop
[
  <<<
 [      loops for second input
   <-     subtracts 1 from first input
   >-
 ]
 adds 48 to result
 +++ +++                        loops 6 times
 [
  <++++ ++++
  >-
 ]
 <. Prints result
  temp0>->
]<<